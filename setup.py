from setuptools import setup

setup(
    name='elope',
    version='0.1',
    description='Trying to bring the elvis operator to python',
    long_description='Elope. An attempt to bring the awesome javascript "elvis operator" to python.',
    classifiers=[
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3.6',
    ],
    keywords='elvis operator javascript conditional safe-navigation-operator',
    url='https://gitlab.com/andrewgy8/elope',
    author='Andrew Graham-Yooll',
    author_email='andrewgy8@gmail.com',
    license='MIT',
    packages=['elope'],
    zip_safe=False,
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
