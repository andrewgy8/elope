from unittest import TestCase
import elope


class TestElvis(TestCase):
    lst = ['1', '2', '3']

    def test_returns_a_true(self):
        e = elope.elvis(True)
        self.assertTrue(e)

    def test_returns_false(self):
        e = elope.elvis(False)
        self.assertFalse(e)

    def test_string_not_existent_in_array(self):
        e = elope.elvis('4' in self.lst)
        self.assertFalse(e)

    def test_string_is_existent_in_array(self):
        e = elope.elvis('1' in self.lst)
        self.assertTrue(e)

