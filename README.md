[![pipeline status](https://gitlab.com/andrewgy8/elope/badges/master/pipeline.svg)](https://gitlab.com/andrewgy8/elope/commits/master)

# EL(vis)OPE(rator)

From [Wikipedia](https://en.wikipedia.org/wiki/Safe_navigation_operator):

> In object-oriented programming, the safe navigation operator (also known as optional chaining operator, 
safe call operator, null-conditional operator) is a binary operator that returns its second argument but null if the first argument is null.


Java: Safe Navigation Operator

Swift: Optional Chaining Operator

Ruby: Lonely Operator

Javascript: Elvis Operator

Groovy: Safe Navigation Operator

C#: Null-conditional Operators

Kotlin: Safe Call Operator

Perl 6: Safe Method Call

CoffeeScript: Existential Operator

Python: ...

You can now do:

```
from elope import elvis

elvis(person.name)
```

Instead of doing this:

```
if person.name:
    return person.name
else:
    pass
```

### Installing

`pip3 install elope`

### Developement

Running tests 

`python setup.py test` 